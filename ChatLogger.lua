blt.forcepcalls(true)

_G = _G or {}
_G.SimpleTools = _G.SimpleTools or {}

chat_log = chat_log or function(message, prefix, color)
    message = message or 'empty'
    prefix = prefix or 'LOG'
    color = color or '00ff00'
    managers.chat:_receive_message(1, tostring(prefix), tostring(message), Color(tostring(color)))
end
