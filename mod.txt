{
	"name" : "Mx Tools - ChatLogger",
	"description" : "A simple log function that prints text in the game chat",
	"author" : "Mxswat",
	"contact" : "/Mxswat",
	"version" : "1.00",
	"priority" : 100,
	"blt_version" : 2,
	"image" : "mxtools.png",
	"color" : "178 135 42",
	"hooks" : [
		{   
			"hook_id" : "lib/entry",
			"script_path" : "ChatLogger.lua"
		}
	],
	"updates" : [
		{
			"identifier" : "mxtools"
		}
	]
}



